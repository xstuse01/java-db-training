package org.but.feec.db.training.api;

import org.but.feec.db.training.data.dao.PersonAddress;
import org.but.feec.db.training.data.dao.PersonContact;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PersonFullDetailViewDto {
    private Long idPerson;
    private LocalDate birthdate;
    private String email;
    private String givenName;
    private String nickname;
    private String familyName;
    private PersonAddress address;
    private List<PersonContact> contacts = new ArrayList<>();

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public PersonAddress getAddress() {
        return address;
    }

    public void setAddress(PersonAddress address) {
        this.address = address;
    }

    public List<PersonContact> getContacts() {
        return contacts;
    }

    public void setContacts(List<PersonContact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "PersonFullDetailViewDto{" +
                "idPerson=" + idPerson +
                ", birthdate=" + birthdate +
                ", email='" + email + '\'' +
                ", givenName='" + givenName + '\'' +
                ", nickname='" + nickname + '\'' +
                ", familyName='" + familyName + '\'' +
                ", address=" + address +
                ", contacts=" + contacts +
                '}';
    }
}
