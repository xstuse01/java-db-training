package org.but.feec.db.training.data.mappers;

import org.but.feec.db.training.data.dao.PersonContact;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonContactsMapper implements RowMapper<PersonContact> {
    @Override
    public PersonContact mapRow(ResultSet rs, int rowNum) throws SQLException {
        PersonContact contact = new PersonContact();
        contact.setContact(rs.getString("contact"));
        contact.setContactType(rs.getString("title"));
        return contact;
    }
}
