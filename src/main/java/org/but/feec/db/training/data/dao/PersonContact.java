package org.but.feec.db.training.data.dao;

public class PersonContact {
    private Long idContact;
    private String contact;
    private String contactType;

    public Long getIdContact() {
        return idContact;
    }

    public void setIdContact(Long idContact) {
        this.idContact = idContact;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    @Override
    public String toString() {
        return "PersonContact{" +
                "idContact=" + idContact +
                ", contact='" + contact + '\'' +
                ", contactType='" + contactType + '\'' +
                '}';
    }
}
