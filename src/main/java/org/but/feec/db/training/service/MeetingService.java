package org.but.feec.db.training.service;

import org.but.feec.db.training.data.dao.Meeting;
import org.but.feec.db.training.data.repository.MeetingRepository;
import org.but.feec.db.training.exceptions.EntityNotFoundException;

import java.util.List;

public class MeetingService {

    private MeetingRepository meetingRepository;

    public MeetingService(MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    public Meeting findById(Long id) {
        Meeting meetingView = meetingRepository.findById(id);
        return meetingView;
    }

    public List<Meeting> findAll() {
        List<Meeting> meetingsView = meetingRepository.findAll();
        return meetingsView;
    }
}
