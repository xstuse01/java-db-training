package org.but.feec.db.training.data.dao;

public class PersonCreateResponseDto {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "PersonCreateResponseDto{" +
                "id=" + id +
                '}';
    }
}
