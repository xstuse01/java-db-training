package org.but.feec.db.training.data.dao;

import java.time.LocalDate;

public class Meeting {

    private Long idMeeting;

    private String note;

    private String place;

    public Long getIdMeeting() {
        return idMeeting;
    }

    public void setIdMeeting(Long idMeeting) {
        this.idMeeting = idMeeting;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public LocalDate getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDate startTime) {
        this.startTime = startTime;
    }

    public LocalDate getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDate endTime) {
        this.endTime = endTime;
    }

    private LocalDate startTime;

    private LocalDate endTime;

    @Override
    public String toString(){
        return "Meeting{" +
                "id=" + idMeeting +
                ", place=" + place +
                ", note=" + note +
                ", start=" + startTime +
                ", stop=" + endTime +
                "}";
    }
}
