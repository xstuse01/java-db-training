package org.but.feec.db.training.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DriverManagerConfig {

    private static final Logger logger = LoggerFactory.getLogger(DriverManagerConfig.class);

    private static Connection connection = null;

    private static final String APPLICATION_PROPERTIES = "application.properties";

    static {
        try (InputStream resourceStream = DataSourceConfig.class.getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES)) {
            Properties properties = new Properties();

            properties.load(resourceStream);

            String url = properties.getProperty("datasource.url");
            String username = properties.getProperty("datasource.username");
            String password = properties.getProperty("datasource.password");
            connection = DriverManager.getConnection(url, username, password);
        } catch (IOException | NullPointerException | IllegalArgumentException e) {
            logger.error("Configuration of the datasource was not successful.", e);
        } catch (SQLException e) {
            logger.error("Could not connect to the database.", e);
        }
    }

    private DriverManagerConfig() {
    }

    public static Connection getConnection() throws SQLException {
        return connection;
    }

}
